﻿using System;
using TestApplication.Game;
using UnityEngine;
using Zenject;

namespace TestApplication.Installers
{
    public class InstallerSceneGame : MonoInstaller
    {
        [SerializeField] private SettingsGame _settingsGame;
        [SerializeField] private ViewPlayerCamera _viewPlayerCamera;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_settingsGame);
            Container.Bind<IViewPlayerCamera>().FromInstance(_viewPlayerCamera);

            Container.Bind<ISettingMap>().To<SettingMapHardCode>().AsSingle();

            Container.Bind<IModelDamage>().To<ModelDamage>().AsSingle();

            BindUnits();

            BindPlayer();
            BindEnemies();
            BindMissles();

            BindFactories();
            BindPools();
        }

        private void BindPlayer()
        {
            Container.Bind(typeof(IInitializable), typeof(IDisposable)).To<ModelCreatePresenterPlayerCamera>()
                .AsSingle().NonLazy();

            Container.Bind(typeof(ModelPlayer), typeof(IModelCreateUnit), typeof(IInitializable), typeof(ITickable))
                .To<ModelPlayer>().AsSingle().NonLazy();

            Container.Bind(typeof(IInput), typeof(ITickable)).To<InputAxis>().AsSingle();
            Container.Bind<ModelPlayerMove>().AsSingle();
            Container.Bind(typeof(ClosestColliderPointGetter), typeof(IDisposable)).To<ClosestColliderPointGetter>()
                .AsSingle();

            Container.Bind<ModelPlayerAttack>().AsSingle();
        }

        private void BindEnemies()
        {
            Container.Bind(typeof(ModelEnemies), typeof(IModelCreateUnit), typeof(IInitializable), typeof(ITickable))
                .To<ModelEnemies>().AsSingle();

            Container.Bind<IStateEnemy>().To<StateEnemyDead>().AsTransient();
            Container.Bind<IStateEnemy>().To<StateEnemyMoving>().AsTransient();

            Container.Bind<IModelMoveEnemy>().To<ModelMoveEnemyStaraight>().AsSingle();
        }

        private void BindUnits()
        {
            Container.Bind(typeof(ModelUnits), typeof(IInitializable), typeof(IDisposable)).To<ModelUnits>().AsSingle();

            Container.Bind(typeof(IInitializable), typeof(IDisposable)).To<ModelCreatePresenterUnit>().AsSingle()
                .NonLazy();

            Container.Bind(typeof(IInitializable), typeof(IDisposable), typeof(ITickable))
                .To<ModelUnitElapseDamageCooldown>().AsSingle().NonLazy();
        }

        private void BindMissles()
        {
            Container.Bind(typeof(IInitializable), typeof(IDisposable)).To<ModelProjectileCreatePresenter>().AsSingle()
                .NonLazy();

            Container.Bind(typeof(ModelProjectiles), typeof(IInitializable), typeof(IDisposable), typeof(ITickable))
                .To<ModelProjectiles>().AsSingle();

            Container.Bind(typeof(ModelProjectileCreateAdapter), typeof(IModelProjectileCreate)).To<ModelProjectileCreateAdapter>()
                .AsSingle();
        }

        private void BindFactories()
        {
            Container.BindFactory<UnitTypes, ModelUnit, FactoryModelUnit>().FromFactory<FactoryModelUnitFromSetting>();
            Container.BindFactory<UnitTypes, IViewUnit, FactoryViewUnit>().FromFactory<FactoryViewUnitFromSetting>();

            Container.BindFactory<ModelUnit, Vector3[], ModelEnemy, FactoryModelEnemy>();

            Container.BindFactory<IViewProjectile, FactoryViewProjectile>()
                .FromComponentInNewPrefab(_settingsGame.PrefabViewProjectile).UnderTransformGroup("Missles");
        }

        private void BindPools()
        {
            Container.BindMemoryPool<ModelProjectile, PoolModelProjectile>();
        }
    }
}