﻿using TestApplication.Start;
using Zenject;

namespace TestApplication.Installers
{
    public class InstallerSceneStart : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IInitializable>().To<ModelSceneStart>().AsSingle().NonLazy();
        }
    }
}