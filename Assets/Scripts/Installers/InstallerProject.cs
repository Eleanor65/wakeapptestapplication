﻿using System;
using TestApplication.Core;
using Zenject;

namespace TestApplication.Installers
{
    public class InstallerProject : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind(typeof(SceneLoader), typeof(IInitializable), typeof(IDisposable)).To<SceneLoader>()
                .AsSingle();
        }
    }
}