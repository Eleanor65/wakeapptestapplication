﻿namespace TestApplication.Core
{
    public enum SceneLoaderStates
    {
        Idle,
        LoadingEmpty,
        LoadingTarget,
    }
}