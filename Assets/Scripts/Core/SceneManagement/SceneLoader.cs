﻿using System;
using UnityEngine.SceneManagement;
using Zenject;

namespace TestApplication.Core
{
    public class SceneLoader : IInitializable, IDisposable
    {
        private SceneLoaderStates _state;
        private string _sceneToLoad;

        public void Initialize()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public void Dispose()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        public void LoadScene(string sceneName)
        {
            _sceneToLoad = sceneName;

            if (_state == SceneLoaderStates.Idle)
            {
                DoLoadScene(SceneNames.Empty);
                _state = SceneLoaderStates.LoadingEmpty;
            }

            switch (_state)
            {
                case SceneLoaderStates.Idle:
                    DoLoadScene(SceneNames.Empty);
                    break;

                case SceneLoaderStates.LoadingEmpty:
                case SceneLoaderStates.LoadingTarget:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DoLoadScene(string sceneName)
        {
            SceneManager.LoadSceneAsync(sceneName);
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            switch (_state)
            {
                case SceneLoaderStates.Idle:
                    break;

                case SceneLoaderStates.LoadingEmpty:
                    DoLoadScene(_sceneToLoad);
                    _state = SceneLoaderStates.LoadingTarget;
                    break;

                case SceneLoaderStates.LoadingTarget:
                    _state = SceneLoaderStates.Idle;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}