﻿namespace TestApplication.Core
{
    public static class SceneNames
    {
        public const string Start = "Start";
        public const string Empty = "Empty";
        public const string Game = "Game";
    }
}