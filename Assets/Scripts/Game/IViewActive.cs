﻿namespace TestApplication.Game
{
    public interface IViewActive
    {
        bool IsActive { get; set; }
    }
}