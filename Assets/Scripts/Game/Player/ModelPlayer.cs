﻿using System;
using Zenject;

namespace TestApplication.Game
{
    public class ModelPlayer : IInitializable, ITickable, IModelCreateUnit
    {
        private readonly FactoryModelUnit _factoryModelUnit;
        private readonly ModelPlayerMove _modelPlayerMove;
        private readonly ModelPlayerAttack _modelPlayerAttack;

        public event Action<ModelUnit> OnCreatedUnit = _ => { };

        public ModelUnit PlayerUnit { get; private set; }

        public ModelPlayer(FactoryModelUnit factoryModelUnit,
            ModelPlayerMove modelPlayerMove,
            ModelPlayerAttack modelPlayerAttack)
        {
            _factoryModelUnit = factoryModelUnit;
            _modelPlayerMove = modelPlayerMove;
            _modelPlayerAttack = modelPlayerAttack;
        }

        public void Initialize()
        {
            PlayerUnit = _factoryModelUnit.Create(UnitTypes.Player);
            OnCreatedUnit(PlayerUnit);
        }

        public void Tick()
        {
            if (_modelPlayerMove.MovePlayer(PlayerUnit))
                return;

            _modelPlayerAttack.TryAttack(PlayerUnit);
        }
    }
}