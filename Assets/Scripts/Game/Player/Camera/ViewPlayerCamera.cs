﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ViewPlayerCamera : MonoBehaviour, IViewPlayerCamera
    {
        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        private IViewCameraShaker _viewCameraShaker;

        private void Awake()
        {
            _viewCameraShaker = GetComponent<IViewCameraShaker>();
        }

        public void Shake()
        {
            _viewCameraShaker?.Shake();
        }
    }
}