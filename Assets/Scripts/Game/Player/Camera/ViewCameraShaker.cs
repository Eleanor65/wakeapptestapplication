﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ViewCameraShaker : MonoBehaviour, IViewCameraShaker
    {
        [SerializeField] private Transform _transformShake;
        [SerializeField] private float _duration = 1;
        [SerializeField] private float _shakeAmount = 1f;

        private float _durationLeft;

        private Vector3 _startPosition;

        private void Awake()
        {
            _startPosition = _transformShake.localPosition;
        }

        private void Update()
        {
            if (_durationLeft <= 0)
                return;

            _durationLeft -= Time.deltaTime;

            if (_durationLeft <= 0)
            {
                _transformShake.localPosition = _startPosition;
                return;
            }

            _transformShake.localPosition = _startPosition + Random.insideUnitSphere * _shakeAmount;
        }

        public void Shake()
        {
            _durationLeft = _duration;
        }
    }
}