﻿using UnityEngine;

namespace TestApplication.Game
{
    public class PresenterPlayerCamera : BasePresenter<ModelUnit, IViewPlayerCamera>
    {
        private readonly Vector3 _heightCamera;

        public PresenterPlayerCamera(ModelUnit model, IViewPlayerCamera view, ModelProjectiles modelProjectiles) : base(model, view)
        {
            var modelPosition = model.Position;

            _heightCamera = (view.Position.y - modelPosition.y) * Vector3.up;
            OnChangedPosition(modelPosition);

            model.OnChangedPosition += OnChangedPosition;

            modelProjectiles.OnMissleHitEnemy += OnMissleHitEnemy;
        }

        private void OnChangedPosition(Vector3 position)
        {
            View.Position = position + _heightCamera;
        }

        private void OnMissleHitEnemy()
        {
            View.Shake();
        }
    }
}