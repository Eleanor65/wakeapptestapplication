﻿namespace TestApplication.Game
{
    public interface IViewCameraShaker
    {
        void Shake();
    }
}