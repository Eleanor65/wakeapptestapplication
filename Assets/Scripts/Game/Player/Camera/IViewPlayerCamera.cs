﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface IViewPlayerCamera
    {
        Vector3 Position { get; set; }

        void Shake();
    }
}