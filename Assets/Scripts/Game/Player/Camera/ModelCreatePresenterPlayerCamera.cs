﻿using System;
using UnityEngine;
using Zenject;

namespace TestApplication.Game
{
    public class ModelCreatePresenterPlayerCamera : IInitializable, IDisposable
    {
        private readonly IModelCreateUnit[] _modelsCreateUnit;
        private readonly IViewPlayerCamera _viewPlayerCamera;
        private readonly ModelProjectiles _modelProjectiles;

        private bool _hasPlayerCamera;

        public ModelCreatePresenterPlayerCamera(IModelCreateUnit[] modelsCreateUnit,
            IViewPlayerCamera viewPlayerCamera,
            ModelProjectiles modelProjectiles)
        {
            _modelsCreateUnit = modelsCreateUnit;
            _viewPlayerCamera = viewPlayerCamera;
            _modelProjectiles = modelProjectiles;
        }

        public void Initialize()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit += OnCreatedUnit;
            }
        }

        public void Dispose()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit -= OnCreatedUnit;
            }
        }

        private void OnCreatedUnit(ModelUnit modelUnit)
        {
            if (modelUnit.Type != UnitTypes.Player)
                return;

            if (_hasPlayerCamera)
            {
                Debug.LogError("There is already one PresenterPlayerCamera. Can't create one more.");
                return;
            }

            _hasPlayerCamera = true;

            //todo should just use factory and let diContainer to inject ModelProjectiles
            //but i'm to lazy to do it right now.
            new PresenterPlayerCamera(modelUnit, _viewPlayerCamera, _modelProjectiles);
        }
    }
}