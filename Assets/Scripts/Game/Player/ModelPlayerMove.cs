﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ModelPlayerMove
    {
        private const float PlayerRadius = .25f;
        private const float TinyTolerance = .001f;

        private readonly IInput _input;
        private readonly ClosestColliderPointGetter _closestColliderPointGetter;

        private readonly int _layerMask;

        public ModelPlayerMove(IInput input, ClosestColliderPointGetter closestColliderPointGetter)
        {
            _input = input;
            _closestColliderPointGetter = closestColliderPointGetter;
            _layerMask = LayerMask.GetMask(LayerConsts.Clip);
        }

        public bool MovePlayer(ModelUnit playerUnit)
        {
            if (playerUnit == null)
                return false;

            if (_input.DirectionMove == Vector3.zero)
                return false;

            var position = playerUnit.Position;
            var direction = playerUnit.Speed * Time.smoothDeltaTime * _input.DirectionMove;
            var targetPosition = position + direction;
            //won't work in sharp conners.
            targetPosition = _closestColliderPointGetter.RecursivePushback(0, 1, targetPosition, PlayerRadius,
                targetPosition, false, _layerMask, TinyTolerance);

            playerUnit.Position = targetPosition;

            return true;
        }
    }
}