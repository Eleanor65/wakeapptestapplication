﻿using System.Linq;
using UnityEngine;

namespace TestApplication.Game
{
    public class ModelPlayerAttack
    {
        private readonly PoolModelProjectile _poolModelProjectile;
        private readonly ModelEnemies _modelEnemies;
        private readonly int _layerMask;
        private readonly int _layerEnemy;
        
        public ModelPlayerAttack(PoolModelProjectile poolModelProjectile, ModelEnemies modelEnemies)
        {
            _poolModelProjectile = poolModelProjectile;
            _modelEnemies = modelEnemies;
            _layerMask = LayerMask.GetMask(LayerConsts.Clip, LayerConsts.Enemy);
            _layerEnemy = LayerMask.NameToLayer(LayerConsts.Enemy);
        }

        public void TryAttack(ModelUnit unit)
        {
            if (!CanAttack(unit))
                return;

            var targetUnit = _modelEnemies.Units.Where(u => u.IsAlive)
                .OrderBy(u => (u.Position - unit.Position).sqrMagnitude).FirstOrDefault();
            if (targetUnit == null)
                return;

            unit.DamageCooldownElapsed = 0f;

            var projectile = _poolModelProjectile.Spawn();
            projectile.Position = unit.Position;
            projectile.Forward = targetUnit.Position - unit.Position;
            projectile.Speed = unit.MissleSpeed;
            projectile.LayerMask = _layerMask;
            projectile.LayerTarget = _layerEnemy;
            projectile.Damage = unit.Damage;
        }

        private bool CanAttack(ModelUnit unit)
        {
            return unit.DamageCooldownElapsed >= unit.DamageCooldown;
        }
    }
}