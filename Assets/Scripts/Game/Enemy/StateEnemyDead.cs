﻿namespace TestApplication.Game
{
    public class StateEnemyDead : BaseStateEnemy
    {
        public override StateEnemyTypes Type => StateEnemyTypes.Dead;

        public override void Activate()
        {
            
        }

        public override void Update()
        {
            
        }
    }
}