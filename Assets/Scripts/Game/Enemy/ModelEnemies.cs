﻿using System;
using System.Collections.Generic;
using Zenject;

namespace TestApplication.Game
{
    public class ModelEnemies : IInitializable, ITickable, IModelCreateUnit
    {
        private readonly ISettingMap _settingMap;
        private readonly FactoryModelUnit _factoryModelUnit;
        private readonly FactoryModelEnemy _factoryModelEnemy;
        private readonly List<ModelEnemy> _modelsEnemy = new List<ModelEnemy>();
        private readonly List<ModelUnit> _units = new List<ModelUnit>();

        public IReadOnlyList<ModelUnit> Units => _units;

        public event Action<ModelUnit> OnCreatedUnit = _ => { };

        public ModelEnemies(ISettingMap settingMap, FactoryModelUnit factoryModelUnit, FactoryModelEnemy factoryModelEnemy)
        {
            _settingMap = settingMap;
            _factoryModelUnit = factoryModelUnit;
            _factoryModelEnemy = factoryModelEnemy;
        }

        public void Initialize()
        {
            foreach (var settingEnemy in _settingMap.SettingsEnemy)
            {
                var unit = _factoryModelUnit.Create(settingEnemy.UnitType);
                unit.Position = settingEnemy.Waypoints[0];
                _units.Add(unit);
                OnCreatedUnit(unit);

                var modelEnemy = _factoryModelEnemy.Create(unit, settingEnemy.Waypoints);
                modelEnemy.SetState(StateEnemyTypes.Moving);
                _modelsEnemy.Add(modelEnemy);
            }
        }

        public void Tick()
        {
            foreach (var modelEnemy in _modelsEnemy)
            {
                modelEnemy.Update();
            }
        }
    }
}