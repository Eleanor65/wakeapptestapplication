﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ModelMoveEnemyStaraight : IModelMoveEnemy
    {
        public Vector3 Move(ModelUnit unit, Vector3 target)
        {
            var direction = target - unit.Position;
            var directionMagnitude = direction.magnitude;

            var moving = unit.Speed * Time.smoothDeltaTime * direction.normalized;
            moving = Vector3.ClampMagnitude(moving, directionMagnitude);
            return unit.Position + moving;
        }
    }
}