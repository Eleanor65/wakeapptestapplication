﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TestApplication.Game
{
    public class ModelEnemy
    {
        private readonly Dictionary<int, IStateEnemy> _states;

        public ModelUnit Unit { get; }
        public Vector3[] Waypoints { get; }

        private IStateEnemy _state;

        public ModelEnemy(ModelUnit unit, IStateEnemy[] states, Vector3[] waypoints)
        {
            Unit = unit;
            Unit.OnChangedHp += OnUnitChangedHp;

            Waypoints = waypoints;

            foreach (var stateEnemy in states)
            {
                stateEnemy.SetModel(this);
            }

            _states = states.ToDictionary(s => (int) s.Type);
        }

        public void Update()
        {
            _state?.Update();
        }

        public void SetState(StateEnemyTypes type)
        {
            if (!_states.TryGetValue((int)type, out var state))
            {
                Debug.LogError($"There is no state with type {type}");
                return;
            }

            state.Activate();
            _state = state;
        }

        private void OnUnitChangedHp(int hp)
        {
            if (hp <= 0)
            {
                SetState(StateEnemyTypes.Dead);
            }
        }
    }
}