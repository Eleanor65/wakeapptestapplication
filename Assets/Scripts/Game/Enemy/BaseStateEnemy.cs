﻿using System;

namespace TestApplication.Game
{
    public abstract class BaseStateEnemy : IStateEnemy
    {
        protected ModelEnemy Model { get; private set; }

        public abstract StateEnemyTypes Type { get; }

        public abstract void Activate();
        public abstract void Update();

        public void SetModel(ModelEnemy model)
        {
            if (Model != null)
            {
                throw new Exception("Model is already set");
            }

            Model = model;
        }
    }
}