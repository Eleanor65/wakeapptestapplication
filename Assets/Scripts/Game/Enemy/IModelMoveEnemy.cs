﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface IModelMoveEnemy
    {
        Vector3 Move(ModelUnit unit, Vector3 target);
    }
}