﻿using System;

namespace TestApplication.Game
{
    public class StateEnemyMoving : BaseStateEnemy
    {
        private const float Tolerance = 0.001f;

        private readonly IModelMoveEnemy _modelMoveEnemy;

        public override StateEnemyTypes Type => StateEnemyTypes.Moving;

        private int _targetWaypoint;
        private bool _movingUpwards = true;

        public StateEnemyMoving(IModelMoveEnemy modelMoveEnemy)
        {
            _modelMoveEnemy = modelMoveEnemy;
        }

        public override void Activate()
        {
            if (Model.Waypoints == null || Model.Waypoints.Length == 0)
            {
                throw new Exception($"There must be at least 1 waypoint for state {this}");
            }

            var index = -1;
            var minDelta = float.MaxValue;
            var position = Model.Unit.Position;

            for (var i = 0; i < Model.Waypoints.Length; i++)
            {
                var waypoint = Model.Waypoints[i];
                var delta = (waypoint - position).sqrMagnitude;
                if (delta >= minDelta)
                    continue;

                minDelta = delta;
                index = i;
            }

            _targetWaypoint = index;
        }

        public override void Update()
        {
            var waypoint = Model.Waypoints[_targetWaypoint];

            var position = _modelMoveEnemy.Move(Model.Unit, waypoint);
            Model.Unit.Position = position;

            if ((position - waypoint).sqrMagnitude < Tolerance)
                SetNextWaypoint();
        }

        private void SetNextWaypoint()
        {
            if (Model.Waypoints.Length == 1)
                return;

            if (_movingUpwards)
            {
                if (_targetWaypoint == Model.Waypoints.Length - 1)
                    _movingUpwards = false;
            }
            else if (_targetWaypoint == 0)
            {
                _movingUpwards = true;
            }

            if (_movingUpwards)
                _targetWaypoint++;
            else
                _targetWaypoint--;
        }
    }
}