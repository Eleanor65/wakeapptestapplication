﻿namespace TestApplication.Game
{
    public interface IStateEnemy
    {
        StateEnemyTypes Type { get; }

        void SetModel(ModelEnemy model);

        void Activate();
        void Update();
    }
}