﻿using System;

namespace TestApplication.Game
{
    public class ModelProjectileCreateAdapter : IModelProjectileCreate
    {
        public event Action<ModelProjectile> OnCreatedMissle = _ => { };

        public void NewMissle(ModelProjectile model)
        {
            OnCreatedMissle(model);
        }
    }
}