﻿using UnityEngine;

namespace TestApplication.Game
{
    public class PresenterProjectile : BasePresenter<ModelProjectile, IViewProjectile>
    {
        public PresenterProjectile(ModelProjectile model, IViewProjectile view) : base(model, view)
        {
            model.OnChangedPosition += OnChangedPosition;
            OnChangedPosition(model.Position);

            model.OnChangedActive += OnChangedActive;
            OnChangedActive(model);
        }

        private void OnChangedPosition(Vector3 position)
        {
            View.Position = position;
        }

        private void OnChangedActive(ModelProjectile model)
        {
            View.IsActive = model.IsActive;
        }
    }
}