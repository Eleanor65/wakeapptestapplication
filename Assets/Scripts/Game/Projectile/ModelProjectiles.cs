﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestApplication.Game
{
    public class ModelProjectiles : IInitializable, IDisposable, ITickable
    {
        private const float MissleRadius = 0.1f;

        private readonly IModelProjectileCreate _modelProjectileCreate;
        private readonly PoolModelProjectile _poolModelProjectile;
        private readonly IModelDamage _modelDamage;
        private readonly ModelUnits _modelUnits;

        private readonly List<ModelProjectile> _modelsActive = new List<ModelProjectile>();
        private readonly Collider[] _colliderBuffer = new Collider[1];
        private readonly int _layerEnemy;

        public event Action OnMissleHitEnemy = () => { }; 

        public ModelProjectiles(IModelProjectileCreate modelProjectileCreate,
            PoolModelProjectile poolModelProjectile,
            IModelDamage modelDamage,
            ModelUnits modelUnits)
        {
            _modelProjectileCreate = modelProjectileCreate;
            _poolModelProjectile = poolModelProjectile;
            _modelDamage = modelDamage;
            _modelUnits = modelUnits;
            _layerEnemy = LayerMask.NameToLayer(LayerConsts.Enemy);
        }

        public void Initialize()
        {
            _modelProjectileCreate.OnCreatedMissle += OnCreatedProjectile;
        }

        public void Dispose()
        {
            _modelProjectileCreate.OnCreatedMissle -= OnCreatedProjectile;
        }

        public void Tick()
        {
            for (var i = _modelsActive.Count -1; i >= 0; i--)
            {
                var projectile = _modelsActive[i];
                var distance = projectile.Speed * Time.smoothDeltaTime;
                var targetPosition = projectile.Position + distance * projectile.Forward;

                var collisionCount = Physics.OverlapCapsuleNonAlloc(projectile.Position, targetPosition, MissleRadius,
                    _colliderBuffer, projectile.LayerMask);

                //if nothing in the way just move
                if (collisionCount == 0)
                {
                    projectile.Position += distance * projectile.Forward;
                    break;
                }

                var collider = _colliderBuffer[0];
                var collGo = collider.gameObject;
                //if not layer enemy just despawn
                if (collGo.layer != projectile.LayerTarget)
                {
                    _poolModelProjectile.Despawn(projectile);
                    break;
                }

                var colliderUnit = collGo.GetComponent<ColliderUnit>();
                if (colliderUnit == null)
                {
                    Debug.LogError(
                        $"Found collider with layer {LayerConsts.Enemy}, but no ColliderUnit component! {collGo}");
                    _poolModelProjectile.Despawn(projectile);
                    break;
                }

                var unitId = colliderUnit.Id;
                var unit = _modelUnits.GetUnit(unitId);
                if (unit == null)
                {
                    Debug.LogError($"Can't get unit with id {unitId}");
                    _poolModelProjectile.Despawn(projectile);
                    break;
                }

                _modelDamage.Damage(projectile, unit);
                _poolModelProjectile.Despawn(projectile);

                if (collGo.layer == _layerEnemy)
                    OnMissleHitEnemy();
            }
        }

        private void OnCreatedProjectile(ModelProjectile model)
        {
            model.OnChangedActive += OnMissleChangedActive;
            OnMissleChangedActive(model);
        }

        private void OnMissleChangedActive(ModelProjectile model)
        {
            if (model.IsActive)
            {
                if (_modelsActive.Contains(model))
                    return;

                _modelsActive.Add(model);
                return;
            }

            _modelsActive.Remove(model);
        }
    }
}