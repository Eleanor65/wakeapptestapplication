﻿using System;

namespace TestApplication.Game
{
    public interface IModelProjectileCreate
    {
        event Action<ModelProjectile> OnCreatedMissle;
    }
}