﻿using System;
using Zenject;

namespace TestApplication.Game
{
    public class ModelProjectileCreatePresenter : IInitializable, IDisposable
    {
        private readonly IModelProjectileCreate _modelProjectileCreate;
        private readonly FactoryViewProjectile _factoryViewProjectile;

        public ModelProjectileCreatePresenter(IModelProjectileCreate modelProjectileCreate,
            FactoryViewProjectile factoryViewProjectile)
        {
            _modelProjectileCreate = modelProjectileCreate;
            _factoryViewProjectile = factoryViewProjectile;
        }

        public void Initialize()
        {
            _modelProjectileCreate.OnCreatedMissle += OnCreatedProjectile;
        }

        public void Dispose()
        {
            _modelProjectileCreate.OnCreatedMissle -= OnCreatedProjectile;
        }

        private void OnCreatedProjectile(ModelProjectile model)
        {
            var view = _factoryViewProjectile.Create();
            new PresenterProjectile(model, view);
        }
    }
}