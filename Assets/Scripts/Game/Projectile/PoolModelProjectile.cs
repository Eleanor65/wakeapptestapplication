﻿using Zenject;

namespace TestApplication.Game
{
    public class PoolModelProjectile : MemoryPool<ModelProjectile>
    {
        private readonly ModelProjectileCreateAdapter _modelProjectileCreateAdapter;

        public PoolModelProjectile(ModelProjectileCreateAdapter modelProjectileCreateAdapter)
        {
            _modelProjectileCreateAdapter = modelProjectileCreateAdapter;
        }

        protected override void OnCreated(ModelProjectile item)
        {
            base.OnCreated(item);

            _modelProjectileCreateAdapter.NewMissle(item);
        }

        protected override void OnSpawned(ModelProjectile item)
        {
            base.OnSpawned(item);

            item.IsActive = true;
        }

        protected override void OnDespawned(ModelProjectile item)
        {
            base.OnDespawned(item);

            item.IsActive = false;
        }
    }
}