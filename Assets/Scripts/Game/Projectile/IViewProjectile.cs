﻿namespace TestApplication.Game
{
    public interface IViewProjectile : IViewPosition, IViewActive
    {
    }
}