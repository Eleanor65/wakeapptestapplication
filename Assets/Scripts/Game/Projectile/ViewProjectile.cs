﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ViewProjectile : MonoBehaviour, IViewProjectile
    {
        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        public bool IsActive
        {
            get => gameObject.activeSelf;
            set
            {
                if (value == gameObject.activeSelf)
                    return;
                gameObject.SetActive(value);
            }
        }
    }
}