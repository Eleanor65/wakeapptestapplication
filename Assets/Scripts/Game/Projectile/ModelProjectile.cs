﻿using System;
using UnityEngine;

namespace TestApplication.Game
{
    public class ModelProjectile
    {
        public Vector3 Position
        {
            get => _position;
            set
            {
                if (value == _position)
                    return;

                _position = value;
                OnChangedPosition(_position);
            }
        }

        public Vector3 Forward
        {
            get => _forward;
            set => _forward = value.normalized;
        }

        public float Speed { get; set; }

        public bool IsActive
        {
            get => _isActive;
            set
            {
                if (value == _isActive)
                    return;

                _isActive = value;
                OnChangedActive(this);
            }
        }

        public int LayerMask { get; set; }
        public int LayerTarget { get; set; }

        public int Damage { get; set; }

        public event Action<Vector3> OnChangedPosition = _ => { };
        public event Action<ModelProjectile> OnChangedActive = _ => { };

        private Vector3 _position;
        private bool _isActive;
        private Vector3 _forward;
    }
}