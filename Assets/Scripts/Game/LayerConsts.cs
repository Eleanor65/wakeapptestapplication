﻿namespace TestApplication.Game
{
    public class LayerConsts
    {
        public const string Clip = "Clip";
        public const string Player = "Player";
        public const string Enemy = "Enemy";
    }
}