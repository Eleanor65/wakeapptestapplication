﻿using System;
using UnityEngine;

namespace TestApplication.Game
{
    public class ModelUnit
    {
        public int Id { get; }
        public string Name { get; }
        public UnitTypes Type { get; }

        public Vector3 Position
        {
            get => _position;
            set
            {
                if (value == _position)
                    return;

                _position = value;
                OnChangedPosition(_position);
            }
        }

        public bool HasHp { get; }
        public int Hp
        {
            get => _hp;
            set
            {
                if (value == _hp)
                    return;

                _hp = value;
                OnChangedHp(_hp);
            }
        }

        public bool IsAlive => Hp > 0;

        public bool HasSpeed { get; }
        public float Speed { get; set; }

        public bool HasDamage { get; }
        public int Damage { get; set; }

        public bool HasDamageCooldown { get; }
        /// <summary>
        /// Cooldown between atacks. Seconds.
        /// </summary>
        public float DamageCooldown { get; set; }
        /// <summary>
        /// Elapsed time after last attack. Seconds.
        /// </summary>
        public float DamageCooldownElapsed { get; set; }

        public float MissleSpeed { get; private set; }

        public event Action<Vector3> OnChangedPosition = _ => { };
        public event Action<int> OnChangedHp = _ => { };

        private Vector3 _position;
        private int _hp;

        public ModelUnit(ModelUnitCreateArgs args)
        {
            Id = args.Id;
            Name = args.Name;
            Type = args.Type;
            Position = args.Position;

            if (args.Hp > 0)
            {
                HasHp = true;
                Hp = args.Hp;
            }

            if (args.Speed > 0f)
            {
                HasSpeed = true;
                Speed = args.Speed;
            }

            if (args.Damage > 0f)
            {
                HasDamage = true;
                Damage = args.Damage;
            }

            if (args.DamageCooldown > 0f)
            {
                HasDamageCooldown = true;
                DamageCooldown = args.DamageCooldown;
            }

            MissleSpeed = args.MissleSpeed;
        }
    }
}