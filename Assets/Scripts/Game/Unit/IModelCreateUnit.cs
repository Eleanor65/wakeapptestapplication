﻿using System;

namespace TestApplication.Game
{
    public interface IModelCreateUnit
    {
        event Action<ModelUnit> OnCreatedUnit;
    }
}