﻿namespace TestApplication.Game
{
    public interface IViewUnit : IViewPosition
    {
        string Name { get; set; }
        int Id { get; set; }

        void Die();
    }
}