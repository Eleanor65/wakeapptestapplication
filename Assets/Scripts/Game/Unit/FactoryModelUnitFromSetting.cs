﻿using Zenject;

namespace TestApplication.Game
{
    public class FactoryModelUnitFromSetting : IFactory<UnitTypes, ModelUnit>
    {
        private readonly SettingsGame _settingsGame;

        private int _unitId;

        public FactoryModelUnitFromSetting(SettingsGame settingsGame)
        {
            _settingsGame = settingsGame;
        }

        public ModelUnit Create(UnitTypes type)
        {
            var setting = _settingsGame.GetSettingUnit(type);

            var id = _unitId++;
            var args = new ModelUnitCreateArgs
            {
                Type = type,
                Id = id,
                Name = $"{type}{id}",
                Damage = setting.Damage,
                DamageCooldown = setting.DamageCooldown,
                Hp = setting.Hp,
                MissleSpeed = setting.ProjectileSpeed,
                Speed = setting.Speed
            };

            return new ModelUnit(args);
        }
    }
}