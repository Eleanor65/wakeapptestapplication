﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace TestApplication.Game
{
    public class ModelUnits : IInitializable, IDisposable
    {
        private readonly IModelCreateUnit[] _modelsCreateUnit;

        private readonly List<ModelUnit> _units = new List<ModelUnit>();

        public ModelUnits(IModelCreateUnit[] modelsCreateUnit)
        {
            _modelsCreateUnit = modelsCreateUnit;
        }

        public void Initialize()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit += OnCreatedUnit;
            }
        }

        public void Dispose()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit -= OnCreatedUnit;
            }
        }

        public ModelUnit GetUnit(int id)
        {
            return _units.FirstOrDefault(u => u.Id == id);
        }

        private void OnCreatedUnit(ModelUnit modelUnit)
        {
            _units.Add(modelUnit);
        }
    }
}