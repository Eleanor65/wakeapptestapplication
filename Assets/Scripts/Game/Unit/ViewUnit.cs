﻿using System.Collections.Generic;
using UnityEngine;

namespace TestApplication.Game
{
    public class ViewUnit : MonoBehaviour, IViewUnit
    {
        private readonly List<ColliderUnit> _colliders = new List<ColliderUnit>();

        public string Name
        {
            get => name;
            set => name = value;
        }

        public int Id
        {
            get => _id;
            set
            {
                if (value == _id)
                    return;

                _id = value;
                foreach (var colliderUnit in _colliders)
                {
                    colliderUnit.Id = _id;
                }
            }
        }

        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        private int _id;

        private void Awake()
        {
            var colliders = GetComponentsInChildren<Collider>();
            foreach (var c in colliders)
            {
                var colliderUnit = c.gameObject.AddComponent<ColliderUnit>();
                colliderUnit.Id = Id;
                _colliders.Add(colliderUnit);
            }
        }

        public void Die()
        {
            gameObject.SetActive(false);
        }
    }
}