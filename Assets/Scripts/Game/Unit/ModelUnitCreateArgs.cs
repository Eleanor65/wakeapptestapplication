﻿using UnityEngine;

namespace TestApplication.Game
{
    public class ModelUnitCreateArgs
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UnitTypes Type { get; set; }
        public Vector3 Position { get; set; }

        public int Hp { get; set; }
        public int Damage { get; set; }
        public float DamageCooldown { get; set; }
        public float MissleSpeed { get; set; }

        public float Speed { get; set; }
    }
}