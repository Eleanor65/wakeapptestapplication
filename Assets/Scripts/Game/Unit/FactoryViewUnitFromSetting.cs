﻿using UnityEngine;
using Zenject;

namespace TestApplication.Game
{
    public class FactoryViewUnitFromSetting : IFactory<UnitTypes, IViewUnit>
    {
        private readonly SettingsGame _settingsGame;

        private Transform Root => _root ?? (_root = new GameObject("Units").transform);
        private Transform _root;

        public FactoryViewUnitFromSetting(SettingsGame settingsGame)
        {
            _settingsGame = settingsGame;
        }

        public IViewUnit Create(UnitTypes type)
        {
            var setting = _settingsGame.GetSettingUnit(type);
            return Object.Instantiate(setting.Prefab, Root);
        }
    }
}