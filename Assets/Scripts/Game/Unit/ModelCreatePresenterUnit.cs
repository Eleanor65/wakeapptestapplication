﻿using System;
using Zenject;

namespace TestApplication.Game
{
    public class ModelCreatePresenterUnit : IInitializable, IDisposable
    {
        private readonly IModelCreateUnit[] _modelsCreateUnit;
        private readonly FactoryViewUnit _factoryViewUnit;

        public ModelCreatePresenterUnit(IModelCreateUnit[] modelsCreateUnit, FactoryViewUnit factoryViewUnit)
        {
            _modelsCreateUnit = modelsCreateUnit;
            _factoryViewUnit = factoryViewUnit;
        }

        public void Initialize()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit += OnCreatedUnit;
            }
        }

        public void Dispose()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit -= OnCreatedUnit;
            }
        }

        private void OnCreatedUnit(ModelUnit model)
        {
            var view = _factoryViewUnit.Create(model.Type);
            new PresenterUnit(model, view);
        }
    }
}