﻿using UnityEngine;

namespace TestApplication.Game
{
    public class PresenterUnit : BasePresenter<ModelUnit, IViewUnit>
    {
        public PresenterUnit(ModelUnit model, IViewUnit view) : base(model, view)
        {
            View.Name = Model.Name;
            View.Id = Model.Id;

            View.Position = model.Position;
            Model.OnChangedPosition += OnChangedPosition;

            Model.OnChangedHp += OnChangedHp;
        }

        private void OnChangedPosition(Vector3 position)
        {
            View.Position = position;
        }

        private void OnChangedHp(int hp)
        {
            if (hp <= 0)
                View.Die();
        }
    }
}