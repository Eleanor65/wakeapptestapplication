﻿using System;
using Unity.Collections;
using UnityEngine;

namespace TestApplication.Game
{
    public class ClosestColliderPointGetter: IDisposable
    {
        private const int PuchbackMaxDepth = 10;

        private readonly Collider[][] _pushBackSphereColliders = new Collider[PuchbackMaxDepth + 1][];

        private NativeArray<RaycastCommand> _raycastCommands;
        private NativeArray<SpherecastCommand> _spherecastCommands;

        private NativeArray<RaycastHit> _raycastHits;

        public ClosestColliderPointGetter()
        {
            _raycastCommands = new NativeArray<RaycastCommand>(1, Allocator.Persistent);
            _spherecastCommands = new NativeArray<SpherecastCommand>(1, Allocator.Persistent);
            _raycastHits = new NativeArray<RaycastHit>(1, Allocator.Persistent);
        }

        public void Dispose()
        {
            _raycastCommands.Dispose();
            _spherecastCommands.Dispose();
            _raycastHits.Dispose();
        }

        /// <summary>
        ///     Check pushback for Sphere
        /// </summary>
        /// <param name="depth">Depth.</param>
        /// <param name="maxDepth">Max iteration depth.</param>
        /// <param name="sphereOrigin">Origin pos.</param>
        /// <param name="radius">Sphere Radius.</param>
        /// <param name="properPos">Proper position.</param>
        /// <param name="isChangeCoordY">If set to <c>true</c> push from other collider with Y coord offset.</param>
        public Vector3 RecursivePushback(int depth, int maxDepth, Vector3 sphereOrigin, float radius, Vector3 properPos,
            bool isChangeCoordY, int collideLayerMask, float tinyTolerance)
        {
            var isContact = false;

            CheckAndCreateColliders(_pushBackSphereColliders);

            var colliders = _pushBackSphereColliders[depth];

            var size = Physics.OverlapSphereNonAlloc(sphereOrigin, radius, colliders, collideLayerMask);
            var directionToContact = Vector3.zero;

            for (var i = 0; i < size; i++)
            {
                var col = colliders[i];

                var position = sphereOrigin;
                var contactPointSuccess = ClosestPointOnSurface(col, position, radius, out var contactPoint);

                if (!contactPointSuccess)
                {
                    Debug.LogErrorFormat(
                        "PlayerMoveController->RecursivePushback. Error: no contact for pos: {0}. radius: {1}", sphereOrigin,
                        radius);
                    return properPos;
                }

                //Для удобного дебага пушбека можно раскомментить.
                //Debug.DrawLine(position, (position + contactPoint)/2, Color.red, 1f);
                //Debug.DrawLine((position + contactPoint) / 2, contactPoint, Color.yellow, 1f);

                directionToContact = contactPoint - position;

                if (directionToContact == Vector3.zero)
                    continue;

                bool facingNormal;
                var maxDistance = directionToContact.magnitude + tinyTolerance;

                // Check which side of the normal we are on
                if (directionToContact.magnitude > tinyTolerance)
                {
                    _spherecastCommands[0] =
                        new SpherecastCommand(position, tinyTolerance, directionToContact.normalized, maxDistance, collideLayerMask);
                    var handle = SpherecastCommand.ScheduleBatch(_spherecastCommands, _raycastHits, 1);
                    handle.Complete();

                    facingNormal = _raycastHits[0].collider != null;
                }
                else
                {
                    _raycastCommands[0] = new RaycastCommand(position, directionToContact.normalized, maxDistance, collideLayerMask);
                    var handle = RaycastCommand.ScheduleBatch(_raycastCommands, _raycastHits, 1);
                    handle.Complete();

                    facingNormal = _raycastHits[0].collider != null;
                }

                // Orient and scale our vector based on side of the normal we are situated
                if (facingNormal)
                {
                    if (Vector3.Distance(position, contactPoint) < radius)
                        directionToContact = (radius - directionToContact.magnitude) * -1 * directionToContact.normalized;
                    else
                        continue;
                }
                else
                {
                    directionToContact = directionToContact.normalized * (radius + directionToContact.magnitude);
                }

                isContact = true;

                if (!isChangeCoordY)
                    directionToContact.y = 0f;

                sphereOrigin += directionToContact;
                properPos += directionToContact;
            }

            if (depth < maxDepth && isContact)
            {
                return RecursivePushback(depth + 1, maxDepth, sphereOrigin, radius, properPos, isChangeCoordY, collideLayerMask,
                    tinyTolerance);
            }

            return properPos;
        }

        public static bool ClosestPointOnSurface(Collider collider, Vector3 to, float radius,
            out Vector3 closestPointOnSurface)
        {
            switch (collider)
            {
                case BoxCollider boxCollider:
                {
                    closestPointOnSurface = ClosestPointOnSurface(boxCollider, to);
                    return true;
                }

                case SphereCollider sphereCollider:
                {
                    closestPointOnSurface = ClosestPointOnSurface(sphereCollider, to);
                    return true;
                }

                case CapsuleCollider capsuleCollider:
                {
                    closestPointOnSurface = ClosestPointOnSurface(capsuleCollider, to);
                    return true;
                }

            }

            Debug.LogError(
                $"{collider.GetType()} does not have an implementation for ClosestPointOnSurface; GameObject.Name='{collider.gameObject.name}'");
            closestPointOnSurface = Vector3.zero;
            return false;
        }

        public static Vector3 ClosestPointOnSurface(SphereCollider collider, Vector3 to)
        {
            var colliderPos = collider.transform.position;
            var p = to - (colliderPos + collider.center);
            p.Normalize();

            p *= collider.radius * collider.transform.localScale.x;
            p += colliderPos + collider.center;

            return p;
        }

        public static Vector3 ClosestPointOnSurface(BoxCollider collider, Vector3 to)
        {
            var ct = collider.transform;

            // Firstly, transform the point into the space of the collider
            var local = ct.InverseTransformPoint(to);

            // Now, shift it to be in the center of the box
            local -= collider.center;

            //Pre multiply to save operations.
            var halfSize = collider.size * 0.5f;

            // Clamp the points to the collider's extents
            var localNorm = new Vector3(
                Mathf.Clamp(local.x, -halfSize.x, halfSize.x),
                Mathf.Clamp(local.y, -halfSize.y, halfSize.y),
                Mathf.Clamp(local.z, -halfSize.z, halfSize.z)
            );

            //Calculate distances from each edge
            var dx = Mathf.Min(Mathf.Abs(halfSize.x - localNorm.x), Mathf.Abs(-halfSize.x - localNorm.x));
            var dy = Mathf.Min(Mathf.Abs(halfSize.y - localNorm.y), Mathf.Abs(-halfSize.y - localNorm.y));
            var dz = Mathf.Min(Mathf.Abs(halfSize.z - localNorm.z), Mathf.Abs(-halfSize.z - localNorm.z));

            // Select a face to project on
            if (dx < dy && dx < dz)
                localNorm.x = Mathf.Sign(localNorm.x) * halfSize.x;
            else if (dy < dx && dy < dz)
                localNorm.y = Mathf.Sign(localNorm.y) * halfSize.y;
            else if (dz < dx && dz < dy) localNorm.z = Mathf.Sign(localNorm.z) * halfSize.z;

            // Now we undo our transformations
            localNorm += collider.center;

            // Return resulting point
            return ct.TransformPoint(localNorm);
        }

        // Courtesy of Moodie
        public static Vector3 ClosestPointOnSurface(CapsuleCollider collider, Vector3 to)
        {
            var ct = collider.transform;
            var colliderCenter = collider.center;

            var lineLength =
                collider.height - collider.radius * 2; // The length of the line connecting the center of both sphere
            var dir = Vector3.up;

            var upperSphere =
                lineLength * 0.5f * dir +
                colliderCenter; // The position of the radius of the upper sphere in local coordinates
            var lowerSphere =
                lineLength * 0.5f * -dir +
                colliderCenter; // The position of the radius of the lower sphere in local coordinates

            var local = ct.InverseTransformPoint(to); // The position of the controller in local coordinates

            var pt = Vector3
                .zero; // The point we need to use to get a direction vector with the controller to calculate contact point

            if (local.y < lineLength * 0.5f && local.y > -lineLength * 0.5f
            ) // Controller is contacting with cylinder, not spheres
                pt = dir * local.y + collider.center;
            else if (local.y > lineLength * 0.5f) // Controller is contacting with the upper sphere 
                pt = upperSphere;
            else if (local.y < -lineLength * 0.5f) // Controller is contacting with lower sphere
                pt = lowerSphere;

            //Calculate contact point in local coordinates and return it in world coordinates
            var p = local - pt;
            p.Normalize();
            p = p * collider.radius + pt;
            return ct.TransformPoint(p);
        }

        private static void CheckAndCreateColliders(Collider[][] colliders)
        {
            if (colliders[0] != null)
                return;

            for (var i = 0; i < colliders.Length; i++)
            {
                colliders[i] = new Collider[PuchbackMaxDepth];
            }
        }
    }
}