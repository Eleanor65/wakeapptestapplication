﻿namespace TestApplication.Game
{
    public class ModelDamage : IModelDamage
    {
        public void Damage(ModelProjectile projectile, ModelUnit unit)
        {
            unit.Hp -= projectile.Damage;
        }
    }
}