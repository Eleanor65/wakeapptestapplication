﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface IInput
    {
        Vector3 DirectionMove { get; }
    }
}