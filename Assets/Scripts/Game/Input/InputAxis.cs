﻿using UnityEngine;
using Zenject;

namespace TestApplication.Game
{
    public class InputAxis : IInput, ITickable
    {
        private const string AxisVertical = "Vertical";
        private const string AxisHorizontal = "Horizontal";

        public Vector3 DirectionMove { get; private set; }

        public void Tick()
        {
            var x = Input.GetAxis(AxisHorizontal);
            var z = Input.GetAxis(AxisVertical);

            var direction = new Vector3(x, 0f, z);
            DirectionMove = Vector3.ClampMagnitude(direction, 1f);
        }
    }
}