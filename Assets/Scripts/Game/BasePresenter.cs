﻿namespace TestApplication.Game
{
    public abstract class BasePresenter<TModel, TVIew>
    {
        protected TModel Model { get; }
        protected TVIew View { get; }

        protected BasePresenter(TModel model, TVIew view)
        {
            Model = model;
            View = view;
        }
    }
}