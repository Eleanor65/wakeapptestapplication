﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface IViewPosition
    {
        Vector3 Position { get; set; }
    }
}