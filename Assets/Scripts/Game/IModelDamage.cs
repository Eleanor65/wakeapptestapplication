﻿namespace TestApplication.Game
{
    public interface IModelDamage
    {
        void Damage(ModelProjectile projectile, ModelUnit unit);
    }
}