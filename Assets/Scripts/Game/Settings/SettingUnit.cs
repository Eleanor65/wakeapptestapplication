﻿using System;
using UnityEngine;

namespace TestApplication.Game
{
    [Serializable]
    public class SettingUnit
    {
        [SerializeField] private UnitTypes _unitType;
        [SerializeField] private ViewUnit _prefab;
        [SerializeField] private int _hp;
        [SerializeField] private int _damage;
        [SerializeField] private float _damageCooldown;
        [SerializeField] private float _projectileSpeed;
        [SerializeField] private float _speed;

        public UnitTypes UnitType => _unitType;
        public ViewUnit Prefab => _prefab;
        public int Hp => _hp;
        public int Damage => _damage;
        public float DamageCooldown => _damageCooldown;
        public float ProjectileSpeed => _projectileSpeed;
        public float Speed => _speed;
    }
}