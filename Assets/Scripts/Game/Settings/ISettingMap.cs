﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface ISettingMap
    {
        Vector3 PositionPlayer { get; }
        SettingEnemy[] SettingsEnemy { get; }
    }
}