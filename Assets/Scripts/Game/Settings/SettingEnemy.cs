﻿using UnityEngine;

namespace TestApplication.Game
{
    public class SettingEnemy : ISettingEnemy
    {
        public Vector3[] Waypoints { get; set; }
        public UnitTypes UnitType { get; set; }
    }
}