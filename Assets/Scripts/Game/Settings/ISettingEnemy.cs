﻿using UnityEngine;

namespace TestApplication.Game
{
    public interface ISettingEnemy
    {
        Vector3[] Waypoints { get; }
        UnitTypes UnitType { get; }
    }
}