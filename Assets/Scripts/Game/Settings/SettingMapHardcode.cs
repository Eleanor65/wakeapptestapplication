﻿using UnityEngine;

namespace TestApplication.Game
{
    public class SettingMapHardCode : ISettingMap
    {
        public Vector3 PositionPlayer => Vector3.zero;
        public SettingEnemy[] SettingsEnemy { get; }

        public SettingMapHardCode()
        {
            SettingsEnemy = new[]
            {
                new SettingEnemy
                {
                    UnitType = UnitTypes.Enemy1,
                    Waypoints = new[] {new Vector3(1, 0, 0), new Vector3(1, 0, 3),}
                },
                new SettingEnemy
                {
                    UnitType = UnitTypes.Enemy1,
                    Waypoints = new[] {new Vector3(0, 0, 3), new Vector3(3, 0, 0),}
                },
                new SettingEnemy
                {
                    UnitType = UnitTypes.Enemy1,
                    Waypoints = new[] {new Vector3(-3, 0, -2), new Vector3(3, 0, -2),}
                },
            };
        }
    }
}