﻿using System;
using System.Linq;
using UnityEngine;

namespace TestApplication.Game
{
    public class SettingsGame : ScriptableObject
    {
        [SerializeField] private SettingUnit[] _settingUnits;
        [SerializeField] private ViewProjectile _prefabViewProjectile;

        public ViewProjectile PrefabViewProjectile => _prefabViewProjectile;

        public SettingUnit GetSettingUnit(UnitTypes type)
        {
            var setting = _settingUnits.FirstOrDefault(su => su.UnitType == type);

            if (setting == null)
            {
                throw new Exception($"There is no SettingUnit with type {type}");
            }

            return setting;
        }
    }
}