﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestApplication.Game
{
    public class ModelUnitElapseDamageCooldown : IInitializable, IDisposable, ITickable
    {
        private readonly IModelCreateUnit[] _modelsCreateUnit;
        private readonly List<ModelUnit> _units = new List<ModelUnit>();

        public ModelUnitElapseDamageCooldown(IModelCreateUnit[] modelsCreateUnit)
        {
            _modelsCreateUnit = modelsCreateUnit;
        }

        public void Initialize()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit += OnOnCreatedUnit;
            }
        }

        public void Dispose()
        {
            foreach (var modelCreateUnit in _modelsCreateUnit)
            {
                modelCreateUnit.OnCreatedUnit -= OnOnCreatedUnit;
            }
        }

        public void Tick()
        {
            foreach (var modelUnit in _units)
            {
                if (modelUnit.DamageCooldownElapsed < modelUnit.DamageCooldown)
                    modelUnit.DamageCooldownElapsed += Time.deltaTime;
            }
        }

        private void OnOnCreatedUnit(ModelUnit unit)
        {
            if (unit.HasDamageCooldown)
            {
                _units.Add(unit);
            }
        }
    }
}