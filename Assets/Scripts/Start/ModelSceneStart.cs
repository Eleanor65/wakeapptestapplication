﻿using TestApplication.Core;
using Zenject;

namespace TestApplication.Start
{
    public class ModelSceneStart : IInitializable
    {
        private readonly SceneLoader _sceneLoader;

        public ModelSceneStart(SceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
        }

        public void Initialize()
        {
            _sceneLoader.LoadScene(SceneNames.Game);
        }
    }
}